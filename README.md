<!--
Copyright (c) 2022-2023 PEReN & CNIL.

MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.-->
API Constat
===========

API d'automatisation de tests et de réalisation de constats sur applications
mobiles.

## Installation du PiRogue sur un Raspberry Pi 4
https://pts-project.org/docs/pirogue/build-a-pirogue/

## Installation du Google Pixel 4a root
https://www.xda-developers.com/google-pixel-4a-how-to-unlock-bootloader-root-pass-safetynet/

A chaque redémarrage, il est nécessaire de redéfinir la valeur du paramètre setenforce sur le téléphone :
```bash
adb shell "su -c 'setenforce 0'"
```

## Installation de l'API

```bash
python3 -m venv .venv
./.venv/bin/pip install -r requirements.txt
npm i
```

## Utilisation

```bash
./.venv/bin/uvicorn api_constat:app --host 127.0.0.1 --port 9026
```
Et ouvrir un navigateur sur [http://127.0.0.1:9026/docs](http://127.0.0.1:9026/docs).

Si l'API tourne sur un pirogue et que vous vous y connectez d'un PC sur le même réseau.

```bash
./.venv/bin/uvicorn api_constat:app --host {IP-Pirogue} --port 9026
```

Et ouvrir un navigateur sur l'IP adéquate.


## Licence

MIT

## Déroulement des tests

L'API va lancer l'application grâce au nom de package passé en argument.
La capture vidéo prend quelques secondes à démarrer, le temps que frida se greffe sur le process d'automatisation.

Le trafic réseau est enregistrer dès le début du test, la capture vidéo est commencé au moment où la fenêtre de notification s'ouvre.
La capture vidéo va durer environ 170 secondes, une fois que la fenêtre de notification s'ouvre à nouveau, la capture vidéo s'est arrêté.

Le test peut néanmoins continuer pendant une durée de 10 minutes.

Le test peut être arrêté à tout moment en revenant sur la page d'acceuil du téléphone. Cette étape fonctionne en détectant la page d'acceuil d'un Pixel 4A.

## Faire un test sur un autre téléphone

Le test peut tout à fait être répliquer sur un autre modèle de téléphone, il suffit de venir éditer la variable ${APP_CLOSED} dans le fichier *default.robot*.

Afin d'obtenir une variable qui convient à votre modèle de téléphone, lancez uiautomatorviewer et cherchez un élement facilement identifiable de la page d'acceuil, pour le Pixel il s'agit de l'élement parent identifié grâce à son nom de paquet.

## Points supplémentaires

- Si jamais l'API ne se clos pas correctement, il se peut que le port soit en cours d'utilisation et provoque une erreur lorsqu'on la relance.
Afin de palier à ce problème, effectuez la commande:

```bash
lsof -i :{API_PORT}
```
Dans notre exemple, API_PORT vaut 9026.
On obtient quelque chose comme suit:

```bash
COMMAND    PID USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
adb     108799   pi    3u  IPv4 531007      0t0  TCP raspberrypi.n7.peren.fr:9026 (LISTEN)
```
Effectuez ensuite la commande:

```bash
kill {PID}
```
