#
# Copyright (c) 2022-2023 PEReN & CNIL.
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.#
import json
import os
import signal
import subprocess
import time
import shutil
import sys
from collections import Counter
from pathlib import Path

from fastapi import FastAPI, HTTPException, UploadFile

from fastapi.responses import FileResponse, RedirectResponse

from .utils import get_test_time, handle_output_folder, parse_http_requests, zipfiles


app = FastAPI()

appium_process = None
WORKING_DIR = Path(os.getcwd())


@app.on_event("startup")
async def startup_event():
    global appium_process
    script_dir = Path(__file__).parent.resolve()
    git_repo_dir = script_dir.parent.parent
    appium_path = git_repo_dir / "node_modules" / ".bin" / "appium"
    appium_process = subprocess.Popen([appium_path])


@app.on_event("shutdown")
async def shutdown_event():
    global appium_process
    appium_process.kill()


@app.get("/", include_in_schema=False)
async def redirect_to_docs():
    """
    Redirect to /docs page.
    """
    return RedirectResponse("./docs")
    

@app.post("/{package}")
async def index(package, reset_app: bool = True, module=None, rules: dict = None):
    handle_output_folder(WORKING_DIR)
    if reset_app:
        try:
            subprocess.check_call(["adb", "shell", "pm", "clear", package])
        except Exception as e:
            message = "Error trying to clear app space for package " + package + " - " + str(e)
            raise HTTPException(status_code=400, detail=message)
    st = time.time()
    pirogue_process = subprocess.Popen(
        ["sudo", "pirogue-intercept-tls", "-U", "-f", package, "-o", "./output"],
        preexec_fn=os.setpgrp,
    )
    if module:
        try:
            arg = f"./api_constat/modules/{package}/{module}.robot"
            if not os.path.isfile(arg):
                os.kill(os.getpgid(pirogue_process.pid), signal.SIGKILL)
                return HTTPException(status_code=400, detail=f"Can't find robot file at path: {arg}")
            subprocess.check_call([sys.executable, "-m", "robot", "--outputdir ./output", arg])
        except Exception as e:
            message = "Error trying to launch robot module " + module + " for package " + package + " - " + str(e)
            os.kill(os.getpgid(pirogue_process.pid), signal.SIGKILL)
            raise HTTPException(status_code=400, detail=message)
    else:
        try:
            subprocess.check_call(
                [sys.executable, "-m", "robot", "--outputdir", "./output", "./api_constat/modules/default.robot"]
            )
        except Exception as e:
            os.kill(os.getpgid(pirogue_process.pid), signal.SIGKILL)
            message = "Error trying to launch package " + package + " - " + str(e)
            raise HTTPException(status_code=400, detail=message)
    subprocess.check_call(["adb", "shell", "am", "force-stop", package])
    os.kill(os.getpgid(pirogue_process.pid), signal.SIGKILL)
    pirogue_process.wait()
    et = time.time()
    time.sleep(10)
    # Some of the files generated are groupe+user root or tcpdump
    subprocess.check_call(["sudo", "chown", "-R", "pi:pi", "./output"])
    subprocess.check_call(
        [
            "editcap",
            "--inject-secrets",
            "tls,./output/sslkeylog.txt",
            "./output/traffic.pcap",
            "./output/decrypted.pcap",
        ]
    )
    # Outputs JSON in Bytes format
    traffic = subprocess.check_output(
        [
            "tshark",
            "-2",
            "-T",
            "json",
            "-t",
            "u",
            "--enable-protocol",
            "communityid",
            "-R",
            "http or http2",
            "--no-duplicate-keys",
            "-Ndmn",
            "-r",
            "./output/decrypted.pcap",
        ]
    )
    # For debugging purpose, can be usefull to dump this
    # with open("./output/traffic.json", "w") as outfile:
    #     outfile.write(traffic.decode('utf-8'))
    request_list = parse_http_requests(json.loads(traffic.decode('utf-8')))
    domains = []
    for request in request_list:
        if isinstance(request["host"], list):
            domains.append(request["host"][0])
        else:
            domains.append(request["host"])
    domains = dict(Counter(domains))
    data = {
        "domains": domains,
        "test_summary": {
            "nb_requests": len(request_list),
            "nb_domains": len(domains),
            "test_duration": int(et - st),
        },
        "all": get_test_time(request_list),
    }
    with open("./output/decrypted.json", "w") as outfile:
        outfile.write(json.dumps(data))
    list_paths = [
        "./output/decrypted.json",
        "./output/video.mp4",
        "./output/decrypted.pcap",
        "./output/output.xml",
    ]
    zipfiles(list_paths)
    return FileResponse(
        "./output/summary.zip",
        media_type="application/x-zip-compressed",
        headers={"Content-Disposition": "attachment;filename=summary.zip"},
    )


@app.post("/process_data/")
async def process(file: UploadFile, rule: str):
    try:
        content = await file.read()
        content = json.loads(content)
    except Exception as e:
        message = "Error trying to read the file. -- " + str(e)
        raise HTTPException(status_code=422, detail=message)
    match = []
    for request in content["all"]:
        if rule.lower() in json.dumps(request).lower():
            match.append(request)
    domains = []
    for request in match:
        if isinstance(request["host"], list):
            domains.append(request["host"][0])
        else:
            domains.append(request["host"])
    domains = dict(Counter(domains))

    data = {"requests": match, "domains": domains}
    return data
