#
# Copyright (c) 2022-2023 PEReN & CNIL.
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.#
import datetime
import json
import os
import shutil
import zipfile

from zipfile import ZipFile
from lxml import etree
from nested_lookup import nested_lookup


def decode_data(hex_data):
    hexa_list = hex_data.split(":")
    int_list = [int(hexa_str, 16) for hexa_str in hexa_list]
    try:
        return json.loads(bytes(int_list))
    except Exception:
        return None


def parse_http2(http2, request):
    ns_search = nested_lookup('http2.headers.method', http2)
    headers_method = ns_search[0] if len(ns_search) >= 1 else None
    if headers_method:
        # header_names et header_values sont deux listes de même longueur
        header_names = nested_lookup("http2.header.name", http2)
        header_values = nested_lookup("http2.header.value", http2)
        # timestamp au format 1675351334.601418000
        timestamp = request.get('_source').get('layers').get('frame').get('frame.time_epoch')
        if timestamp:
            timestamp = timestamp.split('.')[0] + timestamp.split('.')[1][:3]
        ns_search = nested_lookup("http2.headers.authority", http2)
        host = ns_search[0] if len(ns_search) >= 1 else None
        res = {
            "timestamp": timestamp,
            "method": headers_method,
            "host": host,
        }
        headers = {}
        for i, name in enumerate(header_names):
            if name[0] != ':':
                headers[name] = header_values[i]
            if name == ':path':
                res['path'] = header_values[i]
        res["headers"] = headers

        data = nested_lookup("http2.data.data", http2)
        if data:
            for packet in data:
                decoded_data = decode_data(packet)
                if decoded_data:
                    # On suppose si plusieurs bout de données sont présents dans data
                    # Seul un pourra être décodé car il correspond au ré-assemblage de plusieurs paquets
                    res['decode_success'] = True
                    res['data'] = decoded_data
                else:
                    res['decode_success'] = False
                    res['data'] = packet
        json_data = nested_lookup("json", http2)
        if json_data:
            res['json'] = json_data[0]
        return res
    return None


def parse_http1(http, request):
    ns_search = nested_lookup('http.request.method', http)
    headers_method = ns_search[0] if len(ns_search) == 1 else ns_search
    if headers_method:
        ns_search = nested_lookup('http.request.line', http)
        if ns_search:
            headers = {
                request_line.split(":")[0].lower(): request_line.split(":")[1][:-2]
                for request_line in ns_search[0]
            }
        else:
            headers = {
                "headers": "No headers found"
            }
        # timestamp au format 1675351334.601418000
        timestamp = request.get('_source').get('layers').get('frame').get('frame.time_epoch')
        if timestamp:
            timestamp = timestamp.split('.')[0] + timestamp.split('.')[1][:3]
        res = {
            "timestamp": timestamp,
            "method": headers_method,
            "host": headers.pop("host"),
            # "path": http.get("http.request.uri"),
            "headers": headers,
        }
        ns_search = nested_lookup('http.request.uri', http)
        res["path"] = ns_search[0] if len(ns_search) >= 1 else None
        data = http.get("http.file_data")
        if data:
            try:
                data_json = json.loads(data)
                res['decode_success'] = True
                res["data"] = data_json
            except Exception:
                res['decode_success'] = False
                res["data"] = data
        json_data = request.get('_source').get('layers').get('json')
        if json_data:
            res["json"] = json_data
        return res
    return None


def parse_http_requests(request_list):
    new_requests = []
    request_id = 1
    for request in request_list:
        layers = request.get('_source').get("layers")
        if layers:
            http2 = layers.get("http2")
            if http2:
                if not isinstance(http2, list):
                    res = parse_http2(http2, request)
                    if res:
                        res["request_id"] = (9 - len(str(request_id))) * "0" + str(
                            request_id
                        )
                        request_id += 1
                        new_requests.append(res)
            http = layers.get("http")
            if http:
                res = parse_http1(http, request)
                if res:
                    res["request_id"] = (9 - len(str(request_id))) * "0" + str(
                        request_id
                    )
                    request_id += 1
                    new_requests.append(res)
    return new_requests


def add_test_phases(requests):
    test_times = []
    test_names = []
    tree = etree.parse("./output.xml")
    for kw in tree.xpath("/robot/suite/test"):
        test_name = kw.get("name") + " "
        test_names.append(test_name)
        for arg in kw.xpath("arg"):
            if "${" in arg.text:
                test_name += " " + arg.text
        for status in kw.xpath("status"):
            starttime = datetime.datetime.strptime(
                status.get("starttime") + "000", "%Y%m%d %H:%M:%S.%f"
            )
            endtime = datetime.datetime.strptime(
                status.get("endtime") + "000", "%Y%m%d %H:%M:%S.%f"
            )
        test_times.append([test_name, starttime, endtime])
    for request in requests:
        for test_time in test_times:
            if (
                datetime.datetime.fromtimestamp(int(request["timestamp"][:-3]))
                >= test_time[1]
                and datetime.datetime.fromtimestamp(int(request["timestamp"][:-3]))
                < test_time[2]
            ):
                request["test"] = test_time[0]
    return requests, test_names


def zipfiles(filenames):
    zip = ZipFile("./output/summary.zip", mode="w", compression=zipfile.ZIP_DEFLATED)
    for filename in filenames:
        zip.write(filename)
    zip.close()
    return zip


def get_test_time(requests):
    tree = etree.parse("./output/output.xml")
    new_requests = []
    for request in requests:
        # Correspond à l'étape qui lance le Screen Recording
        timestamp_string = (
            tree.xpath("/robot/suite/test/kw")[1].xpath("status")[0].get("endtime")
        )
        timestamp_int = datetime.datetime.timestamp(
            datetime.datetime.strptime(timestamp_string + "000", "%Y%m%d %H:%M:%S.%f")
        )
        request_timestamp = int(request["timestamp"]) / 1000
        request["video_time"] = max(request_timestamp - timestamp_int, 0)
        new_requests.append(request)
    return new_requests

def handle_output_folder(directory):
    output_folder = directory / 'output'
    if os.path.exists(output_folder):
        print('Erasing folder content')
        try:
            shutil.rmtree(output_folder)
            os.mkdir(output_folder)
        except:
            print('Error deleting output content')
    else:
        print('Creating folder output')
        try:
            os.mkdir(output_folder)
        except:
            print('Can not create output folder')


