# Copyright (c) 2022-2023 PEReN & CNIL.
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
*** Settings ***
Library		AppiumLibrary

*** Variables ***
${CONSENT}	id=fr.leboncoin:id/agree
${DISAGREE}	id=fr.leboncoin:id/continueWithoutAgreementText
${SEARCH-BAR}	id=fr.leboncoin:id/search_src_text
${SUGGESTION}       //android.widget.LinearLayout[@bounds="[0,555][1080,665]"]
${LOCATION}	//android.widget.TextView[@bounds="[382,352][649,405]"]
${AROUND-ME}	id=fr.leboncoin:id/aroundMeTextView
${ONLY-THIS-TIME}	id=com.android.permissioncontroller:id/permission_allow_one_time_button
${RANGE-BAR}	id=fr.leboncoin:id/rangeBarView
${SUBMIT-LOCATION}	id=fr.leboncoin:id/searchLocationSubmitButton
${ITEM-IMAGE}	id=fr.leboncoin:id/simpleDraweeViewAd
${VIEW-GALLERY}	id=fr.leboncoin:id/adViewGalleryImage
${FIRST-IMAGE}	id=fr.leboncoin:id/draweeView
${IMAGE}	id=fr.leboncoin:id/viewPager
${CLOSE-PP}	id=fr.leboncoin:id/com_batchsdk_messaging_close_button
${BERCY}	//android.widget.TextView[@text="Bercy (75012)"]

*** Test Cases ***
Open_Application
	Open Application	http://localhost:4723/wd/hub	platformName=Android	automationName=Uiautomator2	autoLaunch=true
Consent
	Wait Until Page Contains Element	${CONSENT}
	Click Element	${CONSENT}	
Search
	Wait Until Page Contains Element	${SEARCH-BAR}
	Click Element	${SEARCH-BAR}
	Wait Until Page Contains Element        ${SEARCH-BAR}
	Input Text      ${SEARCH-BAR}        Yacht
	Wait Until Page Contains Element	${SUGGESTION}
	Click Element	${SUGGESTION}
	Wait Until Page Contains Element        ${LOCATION}
Change Location
	Wait Until Page Contains Element	${LOCATION}
	Click Element	${LOCATION}
	#Wait Until Page Contains Element	${CLOSE-PP}
	#Click Element	${CLOSE-PP}
	#Wait Until Page Contains Element	${AROUND-ME}
	#Click Element	${AROUND-ME}
	Wait Until Page Contains Element	${SEARCH-BAR}
	Input Text      ${SEARCH-BAR}        Bercy
	Wait Until Page Contains Element	${BERCY}
	Click Element	${BERCY}
	#Wait Until Page Contains Element	${ONLY-THIS-TIME}
	#Click Element	${ONLY-THIS-TIME}
	Wait Until Page Contains Element	${RANGE-BAR}
	Swipe	210	795	470	795
	Wait Until Page Contains Element	${SUBMIT-LOCATION}
	Click Element	${SUBMIT-LOCATION}
	Wait Until Page Contains Element        ${ITEM-IMAGE}
Inspect Item
	Wait Until Page Contains Element	${ITEM-IMAGE}
	Click Element	${ITEM-IMAGE}
	Wait Until Page Contains Element	${VIEW-GALLERY}
	Swipe	500	1800	500	1000	2
	Sleep	3
	Swipe	500	1000	500	2000	2
	Wait Until Page Contains Element        ${VIEW-GALLERY}
	Click Element	${VIEW-GALLERY}
	Wait Until Page Contains Element	${FIRST-IMAGE}
	Click Element	${FIRST-IMAGE}
	Wait Until Page Contains Element	${IMAGE}
	Swipe   1000     800     100      800
	Sleep	1
	Swipe   1000     800     100      800
	Sleep	1
Close Application
	Close Application
