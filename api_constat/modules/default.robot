# Copyright (c) 2022-2023 PEReN & CNIL.
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
*** Settings ***
Library		AppiumLibrary

*** Variables ***
${APP_CLOSED}	//android.widget.FrameLayout[@package="com.google.android.apps.nexuslauncher"]


*** Test Cases ***
Open_Application
	Open Application    http://localhost:4723/wd/hub	platformName=Android	automationName=Uiautomator2     autoLaunch=true
User_Test
    Start Screen Recording  1800s
    Open Notifications
    Wait Until Page Contains Element    ${APP_CLOSED}   timeout=1790s
Close_Application
    Stop Screen Recording   video
    Close application
